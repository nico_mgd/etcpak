This is a quick & dirty attempt to add `ktx` output to `etcpak`.

Search `EXPERIMENTAL_KTX` sections to find additions.

It currently only works for `ktx` without mipmaps :
* the ktx `imageSize` is missing for the levels after the first one
